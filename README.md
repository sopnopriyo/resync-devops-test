# DevOps Exercise

!!! Please Fork The Code and not push the code to the repository. !!!

This exercise is to test your familiarity with testing, containerizing the application and then set up a continuous Integration (CI) for it using whichever software that you are familar with.

# Installation

1. Create your own virtual environment.
2. Activate your virtual environment.
3. Install the requirements in the directory pip install -r requirements.txt

# Run the app

The main app is in the folder called app. The main file is app.py. Basically to run the code `python app/main.py` and you can see there being 3 url where 1 is to do simple arithmetic equations and 2 to get users from the json file.

# Testing

What we want to see from you on the testing category is that you have tested every different usecase from users passing in a string for a field that requires integer to basic testing whether the arithmetic equation is done properly, we want to see that.

We can assume that the operation that the users pass in lies within the constraints of ['+', '-', '*', '/'].

# Containers

After you have done the unit test, we then want you to set up with Docker/any containerization platform that you prefer, but preferably Docker to containerize the flask app. The users need to be able to build this app and run the flask command with Docker.

1. You can use any image base
2. Containerize what you need for the application

# CI

We want to see during the Continuous Integration, that you understand how CI works before going to the Continuous Delivery/Continuous Deployment phase.

1. The CI must run the test that you have created. You can choose whichever CI system that you prefer. (TravisCI, Jenkins, Gitlab). Use python tests.py for running the test.
2. Dockerfile must have the testing procedure as well as some kind of test for the file you wrote.

If you have a server that you want to use for Continuous Delivery, feel free to go ahead, we do not require you to do CD for this test.
