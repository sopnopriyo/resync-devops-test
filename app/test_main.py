from .main import app

# --------------------------
#
# Testing the happy flow only
#
# --------------------------


def test_arithmetic_divide():
    response = app.test_client().post(
        '/arithmetic?x=1&y=5&operation=/')
    assert response.status_code == 200
    assert b"0.2" in response.data


def test_arithmetic_multiply():
    response = app.test_client().post(
        '/arithmetic?x=5&y=2&operation=*')
    assert response.status_code == 200
    assert b"10" in response.data


def test_arithmetic_add():
    response = app.test_client().post(
        '/arithmetic?x=10&y=10&operation=+')
    assert response.status_code == 200
    assert b"20" in response.data


def test_arithmetic_minus():
    response = app.test_client().post(
        '/arithmetic?x=10&y=5&operation=-')
    assert response.status_code == 200
    assert b"5" in response.data


def test_get_users():
    response = app.test_client().get('/users')
    assert response.status_code == 200


def test_get_specific_user():
    response = app.test_client().get('/users/geralt')
    assert response.status_code == 200
    assert b"whitewolf" in response.data
