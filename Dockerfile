FROM python:alpine

COPY . /

WORKDIR /app/

RUN pip install -r ../requirements.txt

EXPOSE 5000

ENV FLASK_APP=main.py

CMD flask run
